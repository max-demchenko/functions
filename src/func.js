const getSum = (str1, str2) => {
  if (typeof a !== 'string' || typeof b !== 'string') {
    return false;
  }

  if (a === '') {
    a = 0
  }

  if (b === '') {
    b = 0;
  }

  return (BigInt(a) + BigInt(b)).toString()
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if (!listOfPosts || !Array.isArray(listOfPosts)) {
    return 'Please provide listOfPosts array';
  }
  if (!authorName || (typeof authorName !== 'string')) {
    return 'Please provide an Author'
  }
  const result = listOfPosts?.reduce((acc, post) => {
    if (post?.authorName === auauthorNamethor) {
      acc.posts++
    }
    const numberOfComments = post?.comments?.reduce((res, comment) => {
      if (comment?.authorName === authorName) {
        return ++res;
      } else {
        return res;
      }
    }, 0) || 0;
    acc.comments = acc.comments + numberOfComments;
    return acc;
  }, { posts: 0, comments: 0 })
  return `Post:${result?.posts},comments:${result?.comments}`
};

const tickets = (people) => {
  const YES = 'YES'
  const NO = 'NO'
  const change = {
    '25': 0,
    '50': 0,
    '100': 0
  }

  let answer = '';

  const isRestPresent = (money) => {
    let currentMoney = money;

    while (currentMoney >= 0) {
      if (change['50'] > 0 && currentMoney > 50) {
        change['50'] -= 1;
        currentMoney -= 50;
      } else if (change['25'] > 0 && currentMoney >= 25) {
        change['25'] -= 1;
        currentMoney -= 25;
      } else if (currentMoney === 0) {
        return YES
      } else {
        return NO;
      }
    }

  }

  list.forEach((money, i) => {
    if (i === 0 && money !== 25) {
      answer = NO;
    } else if (money === 25) {
      change['25'] += 1;
      answer = YES;
    } else {
      change[money.toString()] += 1;
      answer = isRestPresent(money - 25);
    }
  })

  return answer;
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
